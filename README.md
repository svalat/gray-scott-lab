Ecole d'été Gray Scott Reloaded - Tests unitaires et profiling mémoire
======================================================================

Ce dépôt contient les sources utilisé comme base d'exercice pour l'école d'été
[Gray Scott Reloaded](https://indico.in2p3.fr/event/30939/overview) organisée en 2024 au [LAPP](https://www.lapp.in2p3.fr/) et différents sites satellites.

Les TP présents ici sont ceux autour des séminaires sur :

1. Les tests unitaires comme méthodologie de développement.
1. Le profiling mémoire.

Profileur mémoire
-----------------

Les TP présents ici gravitent autour des profileur mémoire [MALT](http://memtt.github.com) et [NUMAPROF](http://memtt.github.com).

Les TP seront réalisé pendant l'école dans les container :

```sh
# container basic basé sur ubuntu
docker run -p 8080:8080 -ti \
  registry.gitlab.inria.fr/svalat/gray-scott-lab/mem-ubuntu

# container avancé pour l'école avec VSCode intégré
#
# Note: 
#   - Connectez vous sur le VScode avec le navigateur via http://localhost:8080
#   - Vous trouvez le mot de passe demandé dans la sortie du terminal
#     de lancement du conteneur.
docker run --net=host -p 8080:8080 -ti --rm \
  registry.gitlab.inria.fr/svalat/gray-scott-lab/mem-ubuntu_micromamba_code_server
```

Si vous voulez travailler nativement sur votre Linux, vous pouvez aussi
installer MALT manuellement en suivant les instruction ici :
[installation manuelle](doc/1-install-malt-manual.md).

Passez aux différents TP :

1. [1-tp-tests](1-tp-tests/README.md)
1. [2-tp-malt-memory-profiling](2-tp-malt-memory-profiling/README.md)

Auteurs
-------

- Sébastien Valat (INRIA / LJK) - 2024
