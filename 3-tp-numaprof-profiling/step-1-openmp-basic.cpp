#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <omp.h>
#include "cycle.h"

const size_t WIDTH = 1024;
const size_t HEIGHT = 1024;
const size_t REPEAT = 3;

struct Cell
{
	float u{0};
	float v{0};
};

struct Mesh
{
	float u[HEIGHT][WIDTH];
	float v[HEIGHT][WIDTH];
};

Cell aos[HEIGHT][WIDTH];
Mesh oas;

int main()
{
	memset(aos, 0, sizeof(aos));
	memset(&oas, 0, sizeof(Mesh));

	ticks t0 = getticks();
	for (size_t r = 0 ; r < REPEAT ; r++)
		#pragma omp parallel for
		for (size_t y = 0 ; y < HEIGHT ; y++)
			for (size_t x = 0 ; x < WIDTH ; x++)
				aos[y][x].u += 5.0 + x + y;
	ticks t1 = getticks();

	ticks t2 = getticks();
	for (size_t r = 0 ; r < REPEAT ; r++)
		#pragma omp parallel for
		for (size_t y = 0 ; y < HEIGHT ; y++)
			for (size_t x = 0 ; x < WIDTH ; x++)
				oas.u[y][x] += 5.0 + x + y;
	ticks t3 = getticks();

	size_t threads = omp_get_max_threads();

	printf("AOS = %f\n",(float)(t1-t0) / (float)(WIDTH * HEIGHT * REPEAT / threads));
	printf("OAS = %f\n",(float)(t3-t2) / (float)(WIDTH * HEIGHT * REPEAT / threads));

	return EXIT_SUCCESS;
}

