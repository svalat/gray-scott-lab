# Lab tests unitaires

## installation sur machine perso

Pour utiliser directement sur une machine debian / ubuntu personnelle :

```sh
sudo apt install cmake g++ make libunwind-dev nodejs python3 python3-pip python3-pytest graphviz
```

## Utiliser depuis l'image docker

```sh
docker run -p 8080:8080 -ti registry.gitlab.inria.fr/svalat/gray-scott-lab/mem-ubuntu
```

## TP 1

Trouvez le bug dans MALT dans : `part-1-malt-bug`.

```sh
# dans le docker :
cd /home/jovyan/Examples/part-1-malt-bug

# partie 1
cd part-1-malt-bug

# compiler MALT
mkdir build
cd build
../configure --enable-debug
make -j8

# lancer l'unique test d'intégration
ctest

# voir sa sortie
ctest -V
```

Pour reproduire le bug à la main, générez le profile avec le cas test :

```sh
# jouer manuellement le bug
cd build/tests
LD_PRELOAD=../src/lib/libmalt.so ./test-main

# lancez la vue
malt-webview -H 0.0.0.0 -i malt-test-main-XXXX.json
```

Regardez dans l'interface en allant sur la fonction "call_a" et notez les 0 dans les compteurs.

![MALT bug screenshot](malt-bug-screenshot.png)

Prenez 15 minutes pour essayer de trouver le problème.

Essayez ensuite en activant les tests unitaires dans MALT en mergeant la branche `step-2/unit-test-corrections` :

```sh
# on réactive les tests
../configure --enable-debug --enable-tests

# on active les 
make

# on lance les tests
ctest

# on run ceux qui nous intéressent
ctest -V -R TestXxxxx
```
