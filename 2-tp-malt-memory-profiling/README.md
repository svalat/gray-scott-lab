MALT - cas tests
================

Pour cette session nous allons regarder différents cas de problème que l'on peut
analyser avec MALT.

Nécessaire
----------

Pour ce faire assurez vous d'avoir installer MALT sur votre machine ou d'avoir
récuperer l'image docker fournie pour le TP (pour processeurs x86_64):

```sh
# Ports : 
#  - 8080 : pour VSCode
#  - 8081 : pour l'accès à la vue de MALT
docker run --net=host -p 8080:8080 -ti --rm registry.gitlab.inria.fr/svalat/gray-scott-lab/mem-ubuntu_micromamba_code_server
```

Compilation
-----------

Dans les dockers les sources sont déjà présentes dans :

```sh
cd /home/jovyan/Examples/tp-tests-mem/2-tp-malt-memory-profiling
```

Pour compiler les tests :

```sh
make
```

Analyse
-------

Analysez les cas tests étape par étape :

1. `step-1-basic` : Regardez les différentes informations associées aux cas simplistes.
1. `step-2-problem-A` : Trouvez le problème avec ce code.
1. `step-3-problem-B` : Trouvez le problème avec ce code.
1. `step-4-problem-C` : Trouvez le problème avec ce code.
1. `step-5-problem-D` : Trouvez le problème avec ce code.
1. `step-6-problem-E` : Trouvez le problème avec ce code.
1. `step-7-problem-F` : Trouvez le problème avec ce code.

```sh
# profiler:
malt ./step-1-basic

# visualiser (en ouvrant la vue sur le port 8081)
#
# Note-1: Au premier lancement la commande vous demandera de créer un 
#         LOGIN / PASS pour sécuriser la vue et demandée lorsque vous
#         ouvrez la page dans le navigateur.
#
# Note-2: Vous pouvez l'éviter avec : --no-auth
malt-webview -p 8081 --no-auth -i ./malt-step-1-basic-XXXX.json

# ouvrez la vue dans le navigateur et analysez
firefox http://localhost:8081
```
