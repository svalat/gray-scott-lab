/**********************************************************
*          Project: Gray-Scott School / TP MALT           *
*          Author : Sébastien Valat                       *
*          Date   : 06 / 2024                             *
**********************************************************/

/*********************************************************/
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <list>

/*********************************************************/
void function_doing_things_with_memory(void)
{
//TODO peak
}

/*********************************************************/
int main(void)
{
	function_doing_things_with_memory();
	return EXIT_SUCCESS;
}
