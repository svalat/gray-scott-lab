Installation manuelle
=====================

MALT et NUMAPROF n'ayant pas beaucoup de dépendances, vous pouvez aussi les
installer sur votre Linux facilement.

Ici la procédure sous debian / ubuntu:

```sh
# install deps
sudo apt install -y git cmake g++ make libunwind-dev nodejs clang python3 \
python3-pip python3-pytest wget googletest google-mock libelf-dev hwloc \
nano vim emacs libnuma-dev libnuma1 numactl npm graphviz

# Get pintool (only x86_64) used by NumaProf
wget https://software.intel.com/sites/landingpage/pintool/downloads/pin-3.30-98830-g1d7b601b3-gcc-linux.tar.gz
tar -xvf pin-3.30-98830-g1d7b601b3-gcc-linux.tar.gz

# git clone
git clone https://github.com/memtt/malt.git
git clone https://github.com/memtt/numaprof.git

# create build dirs
mkdir malt/build
mkdir numaprof/build

# build malt
cd malt/build
../configure --prefix=$HOME/usr-gray-scott
make -j8
make install

# build numaprof
cd ../../numaprof/build
../configure --prefix=$HOME/usr-gray-scott --with-pintool=$PWD/../../pin-3.30-98830-g1d7b601b3-gcc-linux
make -j8
make install
```
